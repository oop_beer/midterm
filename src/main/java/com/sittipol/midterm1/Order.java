package com.sittipol.midterm1;

import java.util.Scanner;

public class Order {
    public static void main(String[] args) {
        System.out.println("####Order#####");
        Cafe Product1 = new Cafe("Thai Iced Tea", 35.0);
        Cafe Product2 = new Cafe("English Breakfast Tea", 45.0);
        Cafe Product3 = new Cafe("Cappuccino", 55.0);
        Cafe Product4 = new Cafe("Late", 60.0 );

        int table1 = 1;
        int table2 = 2;
        int table3 = 3;
        int table4 = 4;

        Cafe order1 = new Cafe(table2, Product3);
        Cafe order2 = new Cafe(table3, Product2);
        Cafe order3 = new Cafe(table3, Product4);
        
        System.out.println("Order 1 is for table " + order1.getTable());
        System.out.println("Order 2 is a " + order1.getProduct().getName());
        
        if (order2.getTable() == order3.getTable()) {
            System.out.println("Order 2 & 3 are the same table.");
        }
        order1.setComplete(true);

        System.out.println("####Order status####");
        System.out.println(order1.toString2());
        System.out.println(order2.toString2());
        System.out.println(order3.toString2());
    }


}
