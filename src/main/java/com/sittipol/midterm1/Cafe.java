package com.sittipol.midterm1;

public class Cafe {
    private String name;
    private double price;
    private int table;
    private Cafe product;
    private boolean complete;

    public Cafe(String name, double price) {
        this.name = name;
        this.price = price;

    }

    public Cafe(int table, Cafe product) {
        this.table = table;
        this.product = product;
    }

    public Cafe() {
        complete = false;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String toString() {
        return " " + name + " " + price + " " + "baht";
    }

    public int getTable() {
        return table;
    }

    public Cafe getProduct() {
        return product;
    }

    public void setProduct(Cafe product) {
        this.product = product;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public boolean isComplete() {
        return false;
    }

    public String toString2() {
        String StatusBox;
        if (complete = true)
            StatusBox = "[/]";
        else
            StatusBox = "[ ]";
        return "Table" + table + " :" + product.getName() + StatusBox;
    }

}
